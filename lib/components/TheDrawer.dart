import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:howzit/components/Contact.dart';
import 'package:howzit/components/Helpers.dart';
import 'package:howzit/pages/ChatPage.dart';
import 'package:howzit/pages/ProfilePage.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';
import 'package:flutter_svg/svg.dart';

import '../main.dart';

class TheDrawer extends StatefulWidget{
  var name ;
  // TheDrawer({this.name,}):super(){
  //   name = this.name;
  // }
 
  @override
  _TheDrawer createState() => _TheDrawer();

   
}
class _TheDrawer extends State<TheDrawer> {
  static final Color bgColor = Color(0xffF8F8F8);
  static final rawSvg  = Jdenticon.toSvg(myPublicKey, backColor: colorToString(bgColor));
  
  
  void scan(context) async {
    var result = await BarcodeScanner.scan();

    if (result.type == ResultType.Error){
      toastError("Could not scan the QR Code");
    } else {
      var info = jsonDecode(result.rawContent);

      await conn.send({
        'type': "scanned",
        'fullName': await getFullName()
      } , info['publicKey'], false, null);

      // get contact by publicKey
      var contact = Contact.contacts.firstWhere((element) => element.publicKey == info['publicKey'],
      orElse: () => null);

      if (contact == null) {
        // create new contact
        contact = Contact(
            fullName: info['fullName'],
            publicKey: info['publicKey']
        );

        await Contact.db.save(contact);
      }

      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ChatPage(
            contact: contact,
          ))
      );
    }
  }
  
  
  @override
  void initState() {
    super.initState();
    init();
   
  }
   void init() async {
    var fullName = await getFullName();
    setState(() {
      widget.name = fullName;
    });
  }
  

  @override
  Widget build(BuildContext context) {
    
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
                color: Colors.greenAccent
            ),
            child: Row(
              children:[
                CircleAvatar(
                  radius: 32.0,
                  backgroundColor: Colors.red,
                  child: SvgPicture.string(rawSvg, fit: BoxFit.contain,),
                ),
              
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    navigate(ProfilePage(), false);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal:20.0, vertical:20.0),
                    child: Text((widget.name == null ) ? "Set Your Name" : widget.name.toString(),
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize:20,),),
                  ),
                ),
              ],
            ),
          ),
          ListTile(
              onTap: () {
                Navigator.of(context).pop();
                navigate(ProfilePage(), false);
              },
              leading: Icon(Icons.account_circle),
              title: Text("Profile")
          ),
          ListTile(
            onTap: () {
              scan(context);
            },
            leading:Icon(Icons.add_circle),
            title:Text("Instant chat"),
          )
        ],
      ),
    );
  }
}