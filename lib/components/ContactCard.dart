import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:howzit/pages/ChatPage.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';

import 'Helpers.dart';

class ContactCard extends StatefulWidget{

  ///
  /// {
  ///   'contact': Contact,
  ///   'lastPm': Message,
  ///   'toBeSeen": int
  /// }
  final Map<String, dynamic> contactInfo;
  const ContactCard({Key key, this.contactInfo}) : super(key: key);

  @override
  _ContactCardState createState() => _ContactCardState();
}

class _ContactCardState extends State<ContactCard> {

  String lastPm;
  int lineSize = 30;

  @override
  Widget build(BuildContext context) {
    String rawSvg = Jdenticon.toSvg(widget.contactInfo['contact'].publicKey,
        backColor: colorToString(Colors.white));

    lastPm = widget.contactInfo['lastPm']?.rawText.toString()?? "";

    if (lastPm.length > lineSize) {
      lastPm = lastPm.substring(0, lineSize);
      lastPm += " ...";
    }

    return Container(
      margin :EdgeInsets.only(top: 5.0, bottom: 5.0, right:10.0, left: 10.0),
      padding: EdgeInsets.symmetric(horizontal:10.0, vertical:10.0),
      decoration: BoxDecoration(
          color: Color(0XFFF9FBE7),
          borderRadius: BorderRadius.all(Radius.circular(10))
      ),
      child: ListTile(
        onTap: (){
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ChatPage(
                contact: widget.contactInfo['contact'],
              ))
          );
        },
        leading: SvgPicture.string(rawSvg, fit: BoxFit.contain,),
        title: Text(
          widget.contactInfo['contact']?.fullName ?? ""
          , style:
        TextStyle(
            fontWeight: FontWeight.bold
        ),
        ),
        subtitle: Text(lastPm),
        trailing: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              widget.contactInfo['toBeSeen'] > 0 ?
              Container(
                width: 20.0,
                height: 20.0,
                margin: EdgeInsets.only(top:10),
                decoration: BoxDecoration(
                  // Colors.green
                    color: Color(0xFF66BB6A),
                    borderRadius: BorderRadius.circular(30.0)
                ),
                alignment: Alignment.center,
                child:Text(widget.contactInfo['toBeSeen'].toString()),
              )
              : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}