import 'Database.dart';

class DeliveryLog {
  /// an array of messages that have been received form server
  String uuid; // uuid of received messages

  DeliveryLog({this.uuid});

  static DB db = DB(
    version: 1,
    getDowngradeScript: null,
    tableName: dbTableName,
    inMemoryDb: deliveries,
    getUpgradeDBScript: null,
    toMap: toMap,
    getDBCreateScript: getDBCreateScript,
    updater: updater,
    fromMap: fromMap,
  );

  static Set<DeliveryLog> deliveries = Set();
  static String dbTableName = "logs";

  static String getDBCreateScript(int version) {
    return """
          CREATE TABLE $dbTableName(msgUuid VARCHAR PRIMARY KEY)
    """;
  }
  static Map<String, dynamic> toMap(DeliveryLog obj) {
    return {
      'msgUuid': obj.uuid
    };
  }

  static DeliveryLog fromMap(Map<String, dynamic> map) {
    return DeliveryLog(uuid: map['msgUuid']);
  }

  static updater(){}

  static exists(String uuid){
    return deliveries.firstWhere((element) => element.uuid == uuid,
        orElse: () => null) != null;
  }
}