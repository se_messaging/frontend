import 'package:flutter/cupertino.dart';
import 'package:howzit/components/Database.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';
import 'package:mutex/mutex.dart';
import 'package:path/path.dart';
import 'package:tuple/tuple.dart';

class Contact {
  /// A contact
  ///
  String fullName;
  String publicKey;
  static Mutex _mutex = Mutex();


  static DB db = DB (
    tableName: dbTableName,
    getDowngradeScript: getDowngradeScript,
    getDBCreateScript: getDBCreateScript,
    getUpgradeDBScript: getUpgradeDBScript,
    version: version,
    toMap: toMap,
    inMemoryDb: contacts,
    updater: updateContacts,
    fromMap: fromMap
  );


  static Set<Function> _subscribers = Set();

  static void subscribe(Function f) async {
    await _mutex.acquire();
    /// subscribed functions will be called after each update to messages set
    _subscribers.add(f);

    _mutex.release();
  }

  static void unsubscribe(Function f) async{
    await _mutex.acquire();
    _subscribers.remove(f);
    _mutex.release();
  }


  static void updateContacts() async{
    await _mutex.acquire();
    _subscribers.forEach((_f) {
      _f();
    });
    _mutex.release();
  }

  static Set<Contact> contacts = Set();

  Contact({@required this.fullName, @required this.publicKey});

  Future<void> delete() async {
    contacts.removeWhere((element) => element.publicKey == this.publicKey);
    await db.delete(this);
  }

  static String getDBCreateScript(int version){
    return """
      CREATE TABLE $dbTableName(publicKey TEXT PRIMARY KEY, fullName VARCHAR)
    """;
  }

  static String getUpgradeDBScript(int oldVersion, int newVersion) {
    return "";
  }

  static String getDowngradeScript(int oldVersion, int newVersion) {
    return "";
  }

  static Map<String, dynamic> toMap (Contact contact) {
    return {
      "fullName": contact.fullName,
      "publicKey": contact.publicKey
    };
  }

  static Tuple2<bool, Contact> fromMap(Map<String, dynamic> map) {
    if (map['publicKey'] == null)
      return Tuple2(false, null);

    return Tuple2(true, Contact(
      fullName: map['fullName'],
      publicKey: map['publicKey']
    ));
  }

  static String dbTableName = "contacts";
  static int version = 1;


  @override
  uniqueValue() {
    // a unique value for each object
    return publicKey;
  }

  @override
  String uniqueWhereString() {
    return "publicKey = ?";
  }

  @override
  bool operator==(other) {
    var theOther = other as Contact;
    return (theOther.publicKey == publicKey);
  }

  @override
  int get hashCode {
    return hash(publicKey);
  }

}