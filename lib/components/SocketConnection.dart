import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:howzit/main.dart';
import 'package:tuple/tuple.dart';
import 'package:web_socket_channel/io.dart';
import 'package:mutex/mutex.dart';
import 'Helpers.dart';
import 'Message.dart';

class WebSocketConnection {
  final String url;
  final int timeout;                     // socket connection timeout in seconds, 15 is default
  WebSocket _socket;                     // the underlying socket connection
  IOWebSocketChannel _channel;           // Channel wrapper
  Set<Function> _subscribers;            // subscribers are functions that will be called on data event
  bool connectionEstablished = false;    // a connection is established
  bool isAccepted = false;               // indicates that server accepted the hand shake
  Mutex m = Mutex();

  Function postAccepted;                 // this function will be called after connection accepted


  WebSocketConnection({this.url, this.timeout=15, this.postAccepted}){
    _subscribers = Set();
  }

  String _setHeader(String message){
    /// set the length of the given message as it's header
    /// in the following format
    /// "    length" + message

    int len = message.length;
    String lenStr = len.toString();
    return lenStr.padLeft(10) + message;

  }

  List<String> _parseMessage(String message) {
    /// parse message based on it's header
    /// return a list of actual messages without header

    List<String> messages = List();

    while (message.length > 0){
      try {
        // take the first 10 chars
        String header = message.substring(0, 10);

        // msg len
        int len = int.parse(header);

        String msg = message.substring(10, 10 + len);
        messages.add(msg);

        message = message.substring(10 + len);
      } catch (E) {
        toastError("Could not parse incoming message");
        print(E.toString());
        break;
      }
    }

    return messages;
  }

  void _sendHandShakeMessage(){
    var handShakeData = {
      'type': "connect",
      'me': myPublicKey,
    };

    rawDictSend(handShakeData);
    
  }

  void _clearChannel() {
    if (_channel != null) {
      if (_channel.sink != null) {
        _channel.sink.close();
      }
    }

    if (_socket != null) {
      _socket.close();
    }
  }

  Future<void> establishConnection() async {
    // establish a connection with the server
    try {
      _clearChannel();
      _socket = await WebSocket
          .connect(url)
          .timeout(Duration(seconds: timeout));
      _channel = IOWebSocketChannel(_socket);
      _channel.stream.listen(masterListener, onDone: connectionDropped);

      // do handshake
      // isOn will be set to true after handshake is accepted

      connectionEstablished = true;

      _sendHandShakeMessage();

    } catch(err) {
      connectionEstablished = false;
      ensureConnection();
    }
  }

  Future<void> ensureConnection() async {
    // try to establish a connection if no active connections
    if (!connectionEstablished){
      await establishConnection();
    }
  }

  void connectionDropped() {

    // notify subscribers
    // each subscriber will receive
    // a message indicating that the connection is dropped: todo


//    toastError("Your connection was dropped");

    // web socket connection has dropped
    connectionEstablished = false;
    isAccepted = false;

    ensureConnection();
  }

  Future<void> subscribe(Function func) async {
    await m.acquire();
    try {
      // add function to subscribers set
      _subscribers.add(func);
    } finally {
      m.release();
    }
  }

  Future<void> unsubscribe(Function func) async {
    await m.acquire();
    try {
      // remove function from subscribers list
      _subscribers.removeWhere((sub) => sub == func);
    } finally {
      m.release();
    }
  }

  static String _doEncrypt(Tuple2<dynamic, String> data) {
    return encryptContent( jsonEncode(data.item1) , data.item2);
  }

  static String _doDecrypt(Tuple2<String, String> data) {
    return decryptContent (data.item1, data.item2);
  }

  Future<bool> send(dynamic payload, String destination, bool isCritical, String _uuid) async {
    return await rawDictSend(
      {
        'type': 'send',
        'destination': destination,
        'payload': _doEncrypt(Tuple2(payload, destination) ),
        'is_critical': isCritical,
        'uuid': _uuid
      }
    );
  }

  Future<bool> rawDictSend(dynamic dictMsg) async{
    // send json encoded dictMsg to the server

    await ensureConnection();
    try {
      if (connectionEstablished) {
        _channel.sink.add(
            _setHeader(
                jsonEncode(dictMsg)
            )
        );
        return true;
      }
      else
        return false;
    } catch(E) {
      print(E.toString());
      return false;
    }
  }

  Future<void> masterListener(dynamic incomingMessage) async{
    // master socket listener
    // all the subscribed functions
    // will be called with a json decoded msg

    // during broadcast members cannot change

    // todo: handle error reports

    await m.acquire();

    List<dynamic> messages = _parseMessage(incomingMessage);

    messages.forEach( (msg) async {
      var jsonMsg = jsonDecode(msg);

      if (!isAccepted){
        // check for handshake message
        if (jsonMsg["type"] == "accepted") {
          isAccepted = true;
          if (postAccepted != null)
            postAccepted();
        }
      } else {
        if (jsonMsg['type'] == "msg") {
          jsonMsg['payload'] = jsonDecode( _doDecrypt(Tuple2(jsonMsg['payload'] as String, myPrivateKey)));

          // send delivery report back to server
          this.rawDictSend(
              {
                'type': 'delivery_report',
                'uuid': jsonMsg['uuid']
              }
          );

          _subscribers.forEach((subscriber) {
            subscriber(
                jsonMsg
            );
          });
        } else if (jsonMsg["type"] == "ack") {
          // get message
          var theUuid = jsonMsg['uuid'];

          Message _m = Message.getMessage(theUuid);

          if (_m != null) {
            _m.status = "sent";
            Message.db.save(_m);
          }

        }
      }
    });

    m.release();
  }

  void dispose(){
    _clearChannel();
  }

}