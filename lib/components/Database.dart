import 'package:flutter/cupertino.dart';
import 'package:mutex/mutex.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tuple/tuple.dart';


class DB {
  /// Receives a Serializable object
  /// provides, store, load, edit and delete

  final String tableName;                   // the serializable object
  final Function getDBCreateScript;         // script that creates the database
  final Function getUpgradeDBScript;        // script that upgrades the db
  final Function getDowngradeScript;        // script that down grades the db
  final Function toMap;                     // object to map
  final Function fromMap;                   // object from map
  final int version;                        // current database version
  final Set inMemoryDb;                     // an in memory version of the db
  final Function updater;                   // called after each change to in memory db
  String dbFileName;
  Mutex _mutex;                             // to control concurrency

  DB(
      {
        @required this.tableName,
        @required this.getDBCreateScript,
        @required this.getUpgradeDBScript,
        @required this.getDowngradeScript,
        @required this.version,
        @required this.toMap,
        @required this.fromMap,
        @required this.inMemoryDb,
        @required this.updater,
        dbfile
      }
    ) {
    if (dbfile == null)
      this.dbFileName = "database.db";
    else
      this.dbFileName = dbFileName;
    _mutex = Mutex();

  }

  Future<Database> get db async {
    return await openDatabase(
      // Set the path to the database.
      join(await getDatabasesPath(), tableName),
      // When the database is first created, create a table to store dogs.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          getDBCreateScript(version)
        );
      },
      onUpgrade: (db, oldVersion, newVersion) {
        return db.execute(
          getUpgradeDBScript(oldVersion, newVersion)
        );
      },
      onDowngrade: (db, oldVersion, newVersion) {
        return db.execute(
          getDowngradeScript(oldVersion, newVersion)
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: version,
    );
  }

  Future<void> save(obj) async {
    // save the given obj into db

    await _mutex.acquire();
    var _db = await db;
    await _db.insert(
      tableName,
      toMap(obj),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    // update in memory db
    inMemoryDb.add(obj);

    // notifies subscribers
    updater();

    _mutex.release();
  }

  Future<void> delete(obj) async {
    await _mutex.acquire();

    // delete the given obj from db
    var _db = await db;
    await _db.delete(
      tableName,
      // Use a `where` clause to delete a specific dog.
      where: obj.uniqueWhereString(),
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [obj.uniqueValue()],
    );

    // notifies subscribers
    updater();
    _mutex.release();
  }

  Future<void> saveAll(List objects) async {
    await _mutex.acquire();
    objects.forEach((_obj) async {
      await save(_obj);
    });

    _mutex.release();
  }

  Future<void> loadAll() async {
    await _mutex.acquire();
    var _db = await db;

    final List<Map<String, dynamic>> maps = await _db.query(tableName);
    inMemoryDb.clear();

    maps.forEach((map) {
      Tuple2<bool, dynamic> data = fromMap(map);
      if (data.item1)
      inMemoryDb.add(data.item2);
    });

    updater();
    _mutex.release();

  }

}