import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:howzit/components/Contact.dart';
import 'package:howzit/pages/ChatPage.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';

import 'Helpers.dart';

class SearchContactCard extends StatelessWidget{

  final Contact contact;

  const SearchContactCard({Key key, this.contact}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String rawSvg = Jdenticon.toSvg(contact.publicKey, backColor: colorToString(Colors.white));

    return Container(
      height: 70,
      margin :EdgeInsets.only(top: 5.0, bottom: 5.0, right:10.0, left: 10.0),
      padding: EdgeInsets.symmetric(horizontal:10.0, vertical:10.0),
      decoration: BoxDecoration(
          color: Color(0XFFF9FBE7),
          borderRadius: BorderRadius.all(Radius.circular(10))
      ),
      child: ListTile(
        onTap: () {
          navigate(ChatPage(contact: contact,), false);
        },
        leading: SvgPicture.string(rawSvg, fit: BoxFit.contain,),
        title: Text(contact.fullName, style:
        TextStyle(
            fontWeight: FontWeight.bold
          ),
        ),
      ),
    );
  }
}