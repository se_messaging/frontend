import 'dart:convert';

import 'package:howzit/components/Contact.dart';
import 'package:howzit/components/Database.dart';
import 'package:howzit/components/Helpers.dart';
import 'package:howzit/main.dart';
import 'package:mutex/mutex.dart';
import 'package:path/path.dart';
import 'package:rsa_encrypt/rsa_encrypt.dart';
import 'package:tuple/tuple.dart';
import 'package:uuid/uuid.dart';

class Message {
  /// raw message
  /// auto time stamp on send
  String from;            // sender public key
  String to;              // receiver public key
  DateTime dateTime;      // message send dateTime
  String uuid;            // unique id
  String rawText;         // raw message text
  String status;          // pending/sent/seen

  static Set<Message> messages = Set();           // list of all messages
  static Set<Function> _subscribers = Set();      // list of function that get called each time an update happens

  static DB db = DB(
      getDBCreateScript: dbCreateScript,
      getDowngradeScript: dbDowngradeScript,
      getUpgradeDBScript: dbUpgradeScript,
      toMap: toMap,
      fromMap: fromMap,
      version: version,
      tableName: tableName,
      inMemoryDb: messages,
      updater: updateMessage
  );                         // database handler
  static int version = 1;                         // current db version
  static String tableName = "messages";           // table name

  static Mutex _mutex = Mutex();                  // used to control concurrency

  // construct a message object
  Message({
    this.from, this.to,
    this.rawText, this.uuid,
    this.dateTime, this.status
  }) {

    if (dateTime == null)
      dateTime = DateTime.now();

    if (this.status == null)
      this.status = "pending";

    if (this.uuid == null)
      this.uuid = Uuid().v4();
  }

  /* subscribe/unsubscribe and update
     function that calls all subscribers
  */
  static void subscribe(Function f) async {
    await _mutex.acquire();
    /// subscribed functions will be called after each update to messages set
    _subscribers.add(f);

    _mutex.release();
  }
  static void unsubscribe(Function f) async{
    await _mutex.acquire();
    _subscribers.remove(f);
    _mutex.release();
  }
  static List<String> _shouldSee = List();

  static void updateMessage() async{
    await _mutex.acquire();
    _subscribers.forEach((_f) {
      _f();
    });


    messages.forEach((msg) {
      if (_shouldSee.contains(msg.uuid)) {
        msg.status = "seen";
        _shouldSee.remove(msg.uuid);
        Message.db.save(msg);
      }
    });

    _mutex.release();
  }


  /* database create/upgrade/downgrade sql generators */
  static String dbCreateScript(int version){
    return """
      CREATE TABLE $tableName (
      uuid VARCHAR PRIMARY KEY, 
      sender VARCHAR,
      receiver VARCHAR,
      status VARCHAR,
      create_time DATETIME,
      rawText TEXT,
      seen BOOL)
    """;
  }
  static String dbUpgradeScript(int oldVersion, int newVersion){
    return "";
  }
  static String dbDowngradeScript(int oldVersion, int newVersion){
    return "";
  }

  /* Convert a message object to/from map*/
  static Map<String, dynamic> toMap(Message msg){
    return {
      'sender': msg.from,
      'receiver': msg.to,
      'rawText': msg.rawText,
      'create_time': msg.dateTime.millisecondsSinceEpoch,
      'uuid': msg.uuid,
      'status': msg.status
    };
  }
  static Tuple2<bool, Message> fromMap(Map<String, dynamic> map) {
    Message msg = Message();
    msg.from = map['sender'];
    msg.to = map['receiver'];
    msg.uuid = map['uuid'];

    if (msg.from == null || msg.to == null || msg.uuid == null)
      return Tuple2(false, null);

    msg.status = map['status'];
    msg.rawText = map['rawText'];
    msg.dateTime = DateTime.fromMillisecondsSinceEpoch(map['create_time']);

    return Tuple2(true, msg);
  }

  /* methods used to prepare a message to be sent over the socket */
  Map<String, dynamic> getSendMap() {
    return {
      'type': 'send',
      'destination': to,
      'msg': encryptContent( jsonEncode(
        getBasicMap()
      ) , to)
    };
  }
  Map<String, dynamic> getBasicMap() {
    return {
      'type': 'msg',
      'rawText': rawText,
      'dateTime': dateTime.millisecondsSinceEpoch,
      'uuid': uuid,
      'senderName': myFullName
    };
  }
  static Message fromBasicMap(_map) {
    return Message (
      rawText: _map['payload']['rawText'],
      from: _map['sender'],
      status: 'sent',
      to: myPublicKey,
      dateTime: DateTime.fromMillisecondsSinceEpoch(_map['payload']['dateTime']),
      uuid: _map['payload']['uuid']
    );
  }

  /* some basic message query functions */
  static List<Message> messageOfSender(String senderPk){
    /// all message where senderPk is equal to from
    return messages.where((element) => element.from == senderPk).toList();
  }
  static List<Message> messagesOfReceiver(String receiverPk){
    /// all message where receiverPk is equal to to
    return messages.where((element) => element.to == receiverPk).toList();
  }
  static List<Message> messagesOf(String pk){
    /// all messages where pk is either a sender or a receiver
    Set<Message> msgs = Set();
    msgs.addAll( messageOfSender(pk) );
    msgs.addAll( messagesOfReceiver(pk) );

    return msgs.toList();
  }
  static List<Message> chatsOf(String pk1, String pk2){
    /// all messages between pk1 and pk2
    var messages = messagesOf(pk1).toSet().intersection(messagesOf(pk2).toSet()).toList();
    messages.sort((a, b) => - a.dateTime.millisecondsSinceEpoch + b.dateTime.millisecondsSinceEpoch);

    return messages;
  }
  static List<Message> pendingOf(String pk) {
    /// list of pending messages
    return messages.where((element) => element.status == "pending").toList();
  }
  static List<Message> toBeSeenOf(String pk) {
    /// List of message not yet seen of the given pk
    return messages.where((element) => element.from == pk && element.status == 'sent').toList();
  }

  /* methods to send/receive/see and mark as seen */
  seeAndNotify() async {
    /// mark the current message as seen, and send a seen signal

    status = "seen";
    await db.save(this);

    await conn.send({
      'type': "seen",
      'uuid': uuid,
    }, from, true, Uuid().v4());

  }
  markAsSeen() {
    /// mark as seen
    status = "seen";
    db.save(this);
  }
  send() async {
    /// send the message over the socket
    bool result = await conn.send(getBasicMap(), to, true, uuid);

  }
  static Future<void> sendAllPending() async{
    pendingOf(myPublicKey).forEach((element) async {
      await element.send();
    });
  }

  static Message getMessage(String _uuid) {
    return messages.firstWhere((element) => (element.uuid == _uuid),
        orElse: () => null);
  }

  /* main handler for socket messages related to message*/
  static void generalMessageHandler(dictData) async {
    /// handle seen messages
    /// save incoming into db

    var payload = dictData['payload'];

    if (payload['type'] == "seen") {
      // get the message with it's uuid
      var theMessage = messages.firstWhere(
              (_msg) => _msg.uuid == payload['uuid'],
          orElse: () => null);

      if (theMessage != null){
        theMessage.status = "seen";
        await db.save(theMessage);
      } else {
        _shouldSee.add(payload['uuid']);
      }

    } else if (payload['type'] == 'msg') {
      // create a message object
      var theMessage = Message.fromBasicMap(dictData);
      await db.save(theMessage);

      // if the sender is not in contacts list, add it
      if (
      Contact.contacts.toList().
      firstWhere(
        (element) => element.publicKey == dictData['sender'],
        orElse: () => null
        ) == null
      ) {
        var contact = Contact(
          fullName: payload['senderName'],
          publicKey: dictData['sender']
        );
        await Contact.db.save(contact);
      }

    }

  }

  /* used to uniquely identify a message object in db*/
  String uniqueWhereString() {
    return "uuid = ?";
  }
  uniqueValue() {
    // a unique value for each object
    return uuid;
  }

  @override
  bool operator==(other) {
    Message theOther = other as Message;
    return (theOther.uuid == uuid);
  }

  @override
  int get hashCode {
    return hash(uuid);
  }

}