import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:howzit/main.dart';
import 'package:rsa_encrypt/rsa_encrypt.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pointycastle/api.dart' as crypto;
import 'package:vibration/vibration.dart';
import "package:pointycastle/export.dart";

final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

Future<crypto.AsymmetricKeyPair<crypto.PublicKey, crypto.PrivateKey>> getKeyPair()
{
  var helper = RsaKeyHelper();
  return helper.computeRSAKeyPair(helper.getSecureRandom());
}

crypto.SecureRandom getSeed(String dummy) {
  return RsaKeyHelper().getSecureRandom();
}

AsymmetricKeyPair<PublicKey, PrivateKey> _getRsaKeyPair(
    crypto.SecureRandom secureRandom) {
  /// Set BitStrength to [1024, 2048 or 4096]
  var rsapars = new RSAKeyGeneratorParameters(BigInt.from(65537), 256, 5);
  var params = new ParametersWithRandom(rsapars, secureRandom);
  var keyGenerator = new RSAKeyGenerator();
  keyGenerator.init(params);
  return keyGenerator.generateKeyPair();
}


Future<Map<String, String>> generatePublicPrivateKey() async {
  /// generate public private key pair
  /// return them as a map
  /// { "privateKey": "key", "publicKey": "key"}

  crypto.SecureRandom seed = await compute(getSeed, null);

  var keys = await compute(_getRsaKeyPair, seed);

  return {
    'privateKey': RsaKeyHelper().encodePrivateKeyToPemPKCS1(keys.privateKey),
    'publicKey' : RsaKeyHelper().encodePublicKeyToPemPKCS1(keys.publicKey)
  };

}

Future<String> getFullName() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();

  return prefs.getString("fullName");
}

Future<void> setFullName(String fullName) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();

  await prefs.setString("fullName", fullName);
}


Future<bool> isFirstTime () async{

  SharedPreferences prefs = await SharedPreferences.getInstance();

  myPrivateKey = prefs.getString("privateKey");
  myPublicKey = prefs.getString("publicKey");

  // create private public key pairs if not yet created
  if (  myPrivateKey == null || myPublicKey == null) {
    return true;
  }

  return false;
}

Future<void> initializeKeys() async {
  /* initialize my public/private key */
  bool _isFirstTime = await isFirstTime();

  SharedPreferences prefs = await SharedPreferences.getInstance();

  if (_isFirstTime){
    // generate new pairs
    var keys = await generatePublicPrivateKey();

    // set the global variables
    myPrivateKey = keys['privateKey'];
    myPublicKey = keys['publicKey'];

    // save them to shared prefs
    await prefs.setString("privateKey", myPrivateKey);
    await prefs.setString("publicKey", myPublicKey);
  }

}

void navigate(Widget page, bool clear) {
  if (clear) {
    navigatorKey.currentState.pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => page), (route) => false);
  } else {
    navigatorKey.currentState.push(
      MaterialPageRoute(builder: (context) => page),
    );
  }
}

void toastError(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0);
}

void toastSuccess(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.green,
      textColor: Colors.white,
      fontSize: 16.0);
}

void toastInfo(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.blueAccent,
      textColor: Colors.white,
      fontSize: 16.0);
}

String colorToString(Color color){
  /// convert color object to # based color string
  String s = color.toString();
  s = s.replaceAll("Color(", "").replaceAll("0x", "#").replaceAll(")", "");
  return s;
}

String date(DateTime tm) {
  String month;
  switch (tm.month) {
    case 1:
      month = "january";
      break;
    case 2:
      month = "february";
      break;
    case 3:
      month = "march";
      break;
    case 4:
      month = "april";
      break;
    case 5:
      month = "may";
      break;
    case 6:
      month = "june";
      break;
    case 7:
      month = "july";
      break;
    case 8:
      month = "august";
      break;
    case 9:
      month = "september";
      break;
    case 10:
      month = "october";
      break;
    case 11:
      month = "november";
      break;
    case 12:
      month = "december";
      break;
  }

  return month;
}

String decryptContent(String payload, String privateKey) {
  String finalString = "";
  int chuckSize = 32;

  int chunkCount = (payload.length / chuckSize).ceil();

  if (payload.length % chuckSize != 0) {
    print("BAD PAYLOAD");
    return jsonEncode(payload);
  } else {

    for (var i = 0; i < chunkCount; i++) {
      String _sub;

      _sub = payload.substring(i * chuckSize, i * chuckSize + chuckSize);

      String decrypted = decrypt(
          _sub, RsaKeyHelper().parsePrivateKeyFromPem(privateKey));
      finalString += decrypted;
    }

    return  utf8.decode( jsonDecode( finalString ).cast<int>() );

  }
}

String encryptContent(String raw, String pk) {
  var _pk = RsaKeyHelper().parsePublicKeyFromPem(pk);

  String finalString = "";
  int chuckSize = 31;
  String _raw = jsonEncode(utf8.encode(raw));

  int chunkCount = (_raw.length / chuckSize).ceil();

  for (var i = 0 ; i < chunkCount ; i++) {
    String _sub;

    if (i == chunkCount - 1){
      _sub = _raw.substring(i * chuckSize);
    } else {
      _sub = _raw.substring(i * chuckSize, i * chuckSize + chuckSize);
    }

    var d = encrypt( _sub, _pk);
    finalString += d;
  }

  return finalString;
}


Future<void> showMyDialog(context, String title, Widget body, List<Widget> actions) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              body
            ],
          ),
        ),
        actions: actions
      );
    },
  );
}


void vibrateNewMessage() async{
  if (await Vibration.hasVibrator()) {
    Vibration.vibrate(pattern: [0, 250, 50, 250]);
  }
}