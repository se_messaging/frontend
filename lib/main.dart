import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:howzit/components/Helpers.dart';
import 'package:howzit/components/Message.dart';
import 'package:howzit/components/SocketConnection.dart';
import 'package:howzit/pages/Splash.dart';
import 'package:howzit/pages/Intro.dart';

import 'components/Contact.dart';
import 'components/DeliveryLogs.dart';


String myPrivateKey;        // current users public key
String myPublicKey;         // current users private key
String myFullName;          // current users full name

WebSocketConnection conn;
bool isFirstTimeV;
bool save = true;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  isFirstTimeV = await isFirstTime();
  String host = "wss://app.howzit.ir";
  conn = WebSocketConnection(url: host, postAccepted: Message.sendAllPending);
  conn.subscribe(Message.generalMessageHandler);
  conn.establishConnection();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'Howzit',
      theme: ThemeData(
        backgroundColor: Color(0xffF8F8F8),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: isFirstTimeV ? Tutorial() : SplashScreen(),
    );
  }
}

Future<void> initAllSystems() async {
  await initializeKeys();
  myFullName = await getFullName();
  await Message.db.loadAll();
  await Contact.db.loadAll();
  await DeliveryLog.db.loadAll();
}


