import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:howzit/components/Contact.dart';
import 'package:howzit/components/Helpers.dart';
import 'package:howzit/main.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'ChatPage.dart';


class ProfilePage extends StatefulWidget{
  final bool withScaffold;

  ProfilePage({this.withScaffold = true});

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  static final Color bgColor = Color(0xFF69F0AE);
  static final rawSvg  = Jdenticon.toSvg(myPublicKey, backColor: colorToString(Colors.white));

  final fullNameController = TextEditingController();

  void genericHandler(data) {
    var name = data['payload']['fullName'];
    if (data['payload']['type'] == "scanned") {
      showMyDialog(context, "Instant Chat Request",
          Text("Would You Like to Chat With " + (name == null ? "NO NAME" : name))
          ,
          [
            FlatButton(
              child: Text("Let's chat now"),
              onPressed: () async {

                // get contact by publicKey
                var contact = Contact.contacts.firstWhere((element) => element.publicKey == data['sender'],
                orElse: () => null);

                if (contact == null) {
                  // create new contact
                  contact = Contact(
                      fullName: data['payload']['fullName'],
                      publicKey: data['sender']
                  );

                }

                await Contact.db.save(contact);

                navigate(ChatPage(
                  contact: contact,
                ), false);
              },
            ),
            FlatButton(
              child: Text('Later'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ]
      );
    }
  }


  @override
  void initState() {
    super.initState();
    conn.subscribe(genericHandler);
    init();
  }

  void init() async {
    var fullName = await getFullName();

    setState(() {
      fullNameController.text = fullName == null ? "" : fullName;
    });
  }

  void saveName() async {
    await setFullName(fullNameController.text.toString());
    setState(() {

    });
  }

  @override
  void dispose() {
    super.dispose();
    conn.unsubscribe(genericHandler);
  }

  @override
  Widget build(BuildContext context) {
    Widget _raw = Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            margin: EdgeInsets.only(top: 20, bottom: 20),
            child: Text("People Can Chat With You\n by Scanning Your QR Code",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
          Expanded(
              flex: 2,
              child: Container(
                child: QrImage(
                  data: jsonEncode(
                      {
                        'fullName': fullNameController.text.toString(),
                        'publicKey': myPublicKey
                      }
                  ),
                  version: QrVersions.auto,
                ),
              )
          ),
          Expanded(
            flex: 1,
            child: Container(
                height: 400,
                width: 400,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Color(0XFFF9FBE7),
                ),
                margin: EdgeInsets.all(15),
                child:Center(
                  child: TextField(
                    style: TextStyle(
                        fontSize: 20
                    ),
                    controller: fullNameController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(10),
                      border: InputBorder.none,
                      hintText: 'Enter your full name ...',
                      hintStyle: TextStyle(fontSize: 17),
                    ),
                    textAlign: TextAlign.center,
                    onChanged: (text) {
                      saveName();
                    },
                  ),
                )
            ),
          )
        ],
      ),
    );

    if (!widget.withScaffold)
      return _raw;

    return Scaffold(
      appBar: AppBar(
        iconTheme: new IconThemeData(color: Colors.black),
        backgroundColor: bgColor,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 20.0,
                child: SvgPicture.string(rawSvg, fit: BoxFit.contain,),
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: Text(fullNameController.text, style: TextStyle(
                    color: Colors.black
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      body: _raw,
    );

  }
}