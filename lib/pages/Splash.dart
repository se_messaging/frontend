import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:howzit/components/Contact.dart';
import 'package:howzit/components/DeliveryLogs.dart';
import 'package:howzit/components/Helpers.dart';
import 'package:howzit/components/Message.dart';
import 'package:howzit/main.dart';
import 'package:howzit/pages/HomePage.dart';
import 'dart:async';

class SplashScreen extends StatefulWidget{
  @override
  _SplashScreenState createState() => _SplashScreenState();

}

class _SplashScreenState extends State<SplashScreen>{

  _SplashScreenState() {
    initSystems();
  }

  Future<void> initSystems() async {
    await initAllSystems();
    navigate(HomePage(), true);
  }

  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Colors.white),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.greenAccent,
                        radius: 60.0,
                        child: Icon(
                          Icons.mail_outline,
                          color: Colors.white,
                          size: 60.0,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top:10.0),
                      ),
                      Text(
                        "HOWZIT",
                        style: TextStyle(
                          color: Colors.greenAccent,
                          fontSize: 32.0,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Padding(
                        padding: EdgeInsets.only(top: 20.0)
                    ),
                    Text(
                      isFirstTimeV ? "First Time Setup"
                      : "Loading",
                      style: TextStyle(
                          color: Colors.greenAccent,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      child: isFirstTimeV ? Text(
                        "This Might Take a While",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold
                        ),
                      ) : SizedBox() ,
                    )
                  ],
                ),
              ),

              Expanded(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.greenAccent
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        child: Container(
                          child: Center( child:
                          Text(
                            "Secure End-to-End Message Passing",
                            style: TextStyle(
                                color: Colors.white
                            ),
                          )
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
