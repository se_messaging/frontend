import 'dart:convert';
import 'package:barcode_scan/gen/protos/protos.pbenum.dart';
import 'package:barcode_scan/platform_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:howzit/components/Contact.dart';
import 'package:howzit/components/Helpers.dart';
import 'package:howzit/components/TheDrawer.dart';
import 'package:howzit/main.dart';
import 'package:howzit/pages/ChatPage.dart';
import 'package:howzit/pages/ProfilePage.dart';
import 'package:howzit/pages/RecentChats.dart';
import 'package:howzit/pages/SearchContact.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _indexAt;
  @override
  void initState() {
    super.initState();
    if (isFirstTimeV)
      _indexAt = 0;
    else
      _indexAt = 1;
  }

  void scan(context) async {
    var result = await BarcodeScanner.scan();

    if (result.type == ResultType.Error){
      toastError("Could not scan the QR Code");
    } else {
      var info = jsonDecode(result.rawContent);

      await conn.send({
        'type': "scanned",
        'fullName': await getFullName()
      } , info['publicKey'], false, null);

      // get contact by publicKey
      var contact = Contact.contacts.firstWhere((element) => element.publicKey == info['publicKey'],
          orElse: () => null);

      if (contact == null) {
        // create new contact
        contact = Contact(
            fullName: info['fullName'],
            publicKey: info['publicKey']
        );

        await Contact.db.save(contact);
      }

      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ChatPage(
            contact: contact,
          ))
      );
    }
  }


  void _onItemTapped(int index) {
    if (index == 2)
      scan(context);
    else
      setState(() {
        _indexAt = index;
      });
  }

  List _pages = [
    ProfilePage(withScaffold: false,),
    RecentChats(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      drawer: TheDrawer(),
      appBar: AppBar(
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: Icon(Icons.menu, color: Colors.white,),
            onPressed: () => Scaffold.of(context).openDrawer(),
          );
        }),
        backgroundColor: Color(0xFF69F0AE),
        centerTitle: true,
        title: Text("HOWZIT", style: TextStyle(color: Colors.white),),
        actions: [
          Container(
            width: 45,
            decoration: ShapeDecoration(
            shape: CircleBorder(),
            ),
            child: IconButton(
              onPressed: () {
                showSearch(context: context, delegate: DataSearch());
              },
              icon: Icon(Icons.search, color: Colors.white,),
            ),
          ),
        ],
      ),
      body: Container(
        margin: EdgeInsets.only(top:20),
        child: Column(
          children :<Widget>[
            Expanded(
              flex: 1,
              child: _pages.elementAt(_indexAt)
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            title: Text('Profile'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.message),
            title: Text('Chats'),
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            title: Text('Instant Chat'),
          ),
        ],
        selectedItemColor: Color(0xFF69F0AE),
        currentIndex: _indexAt,
        onTap: _onItemTapped,

      ),
  );
  }
}