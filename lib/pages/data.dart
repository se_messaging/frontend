class SliderModel{
  String imagePath, title, description;
  SliderModel({this.imagePath,this.title,this.description});

  void setImageAssetPath(String path){
    imagePath = path;
  }
  void setTitle(String _title){
    title = _title;
  }
  void setDescription(String _description){
    description = _description;
  }

  String getImageAssetPath(){
    return imagePath;
  }
  String getTitle(){
    return title;
  }
  String getDescription(){
    return description;
  }

}


List<SliderModel> getSlides(){
  List<SliderModel> slides = new List<SliderModel>();
  SliderModel sliderModel = new SliderModel();
  // first model
  sliderModel.setImageAssetPath("image/secure.png");
  sliderModel.setTitle("_title");
  sliderModel.setDescription("Secure End-To-End Encrypted\n Messaging Application");
  slides.add(sliderModel);

  SliderModel sliderModel2 = new SliderModel();
  // second model
  sliderModel2.setImageAssetPath("image/barcode.png");
  sliderModel2.setTitle("_title2");
  sliderModel2.setDescription("You Can Always Start Chatting With Others\n by Scanning Their QR Code!\n No Registrations required");
  slides.add(sliderModel2);

  SliderModel sliderModel3 = new SliderModel();
  // third model
  sliderModel3.setImageAssetPath("image/cert.png");
  sliderModel3.setTitle("_title3");
  sliderModel3.setDescription("Your Communication Keys Are Generated Offline\n Without Interference of Any Other Third Parties");
  slides.add(sliderModel3);

  return slides;
}