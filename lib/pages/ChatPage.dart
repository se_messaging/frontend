import 'dart:async';
import 'package:auto_direction/auto_direction.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:howzit/components/Contact.dart';
import 'package:howzit/components/Helpers.dart';
import 'package:howzit/components/Message.dart';
import 'package:howzit/main.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';

class ChatPage extends StatefulWidget{

  final Contact contact;

  ChatPage({@required this.contact});

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final msgTextController = TextEditingController();              // message text controller
  final Color bgColor = Color(0xFF69F0AE);                        // background color
  ScrollController _scrollController = new ScrollController();    // scroll controller
  List<Message> messages = List();                                // list of messages being displayed
  bool keyboardOpen = false;                                      // is keyboard open
  String rawSvg;                                                  // raw svg of the contact

  bool isTyping = false;                                          // is typing
  Timer _timer;
  String currentText =  "";

  void startTyping(dictData) {
    // contact started typing
    if (dictData['sender'] == widget.contact.publicKey
    && dictData['payload']['type'] == 'typing') {
      if (_timer != null) {
        _timer.cancel();
      }

      setState(() {
        _timer = Timer(Duration(milliseconds: 500), () {
          setState(() {
            isTyping = false;
            _nameController.text = widget.contact.fullName;
          });
        });

        isTyping = true;
        _nameController.text = widget.contact.fullName + " is typing ...";
      });
    }
  }

  void sendTyping() {
    // send start typing signal to contact
    conn.send({
      'type': "typing",
    }, widget.contact.publicKey, false, null);
  }
  TextEditingController _nameController = TextEditingController();

  @override
  void initState() {
    // generate identicon
    rawSvg = Jdenticon.toSvg(widget.contact.publicKey,);

    // subscribe to changes in messages
    Message.subscribe(messagesUpdated);
    conn.subscribe(startTyping);

    // load conversation with the contact
    loadMessages();

    _nameController.text = widget.contact.fullName == null ? "" : widget.contact.fullName;

    super.initState();
  }

  /* scroll methods */
  void sortScroll() {
    setState(() {
      messages.sort((a, b) => - a.dateTime.millisecondsSinceEpoch + b.dateTime.millisecondsSinceEpoch);
      scrollToEnd();
    });
  }
  void scrollToEnd() {
    setState(() {
      _scrollController.animateTo(0.0,
          duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    });
  }

  /* send/receive/load messages */
  void send() async {
    if (msgTextController.text.trim() == "") {
      msgTextController.text = "";
      return;
    }

    Message theMessage = Message (
      from: myPublicKey,
      to: widget.contact.publicKey,
      rawText: msgTextController.text.toString().trim()
    );

    msgTextController.text = "";

    await Message.db.save(theMessage);
  }
  void messagesUpdated(){
    loadMessages();
  }
  void loadMessages() async {

    Message.toBeSeenOf(widget.contact.publicKey).forEach((element) {
      element.seeAndNotify();
    });

    await Message.sendAllPending();

    setState(() {
      messages = Message.chatsOf(myPublicKey, widget.contact.publicKey).toList();

      sortScroll();
    });

  }


  void handleKeyboardOpen(){
    /// scroll the message list when keyboard opens
    /// to avoid hiding messages behind the keyboard


    if ( MediaQuery.of(context).viewInsets.bottom != 0 && !keyboardOpen) {
      try {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent)
          // if keyboard is not already open and the user is looking at the end of the conversation
          // and user taps on the text box to write a message, scroll the list view to the end and
          // set keyboard state to open

          scrollToEnd();
        setState(() {
          keyboardOpen = true;
        });
      } catch (E) {
    }

    } else if (MediaQuery.of(context).viewInsets.bottom == 0) {
      // set keyboard status to false when keyboard closes
      setState(() {
        keyboardOpen = false;
      });
    }
  }

  @override
  void dispose() {
    Message.unsubscribe(messagesUpdated);
    conn.unsubscribe(startTyping);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    handleKeyboardOpen();

    return Scaffold(

      appBar: AppBar(
        backgroundColor: bgColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                child: SvgPicture.string(rawSvg)
              ),
            ),
            Expanded(
              flex: 5,
              child: Container(
                margin: EdgeInsets.all(10),
                child: TextField(
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),
                  controller: _nameController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Enter contact name here',
                  ),
                  onChanged: (newName) {
                    setState(() {
                      widget.contact.fullName = newName;
                      Contact.db.save(widget.contact);
                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),

      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Container(
                  child: ListView.builder(
                    controller: _scrollController,
                    shrinkWrap: true,
                    reverse: true,
                    itemCount: messages.length,
                    itemBuilder: (context, index) {
                      bool isMe = messages[index].from == myPublicKey;
                      var alignment = isMe ? Alignment.centerRight : Alignment.centerLeft;
                      return Align(
                        alignment: alignment,
                        child: Container(
                          margin: EdgeInsets.all(10),
                          constraints: BoxConstraints(
                              maxWidth: 250
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(

                                decoration: BoxDecoration(
                                    color: isMe ? Color(0Xffd2f3e0) : Color(0xffeaeaea),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.9),
                                        spreadRadius: 0.01,
                                        blurRadius: 2,
                                        offset: Offset(0, 3), // changes position of shadow
                                      ),
                                    ],
                                ),
                                child: ListTile(
                                  title: AutoDirection(
                                    text: messages[index].rawText,
                                    child: Text(messages[index].rawText)
                                  ),
                                ),
                              ),
                              Align(
                                alignment: alignment,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    isMe ? Container(
                                      margin: EdgeInsets.all(5),
                                      child: Text(
                                        messages[index].status == "sent" ?
                                        "✓" : messages[index].status == "seen" ?
                                        "✓✓" :  "sending...",
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic
                                        ),
                                      ),
                                    ) : SizedBox(),
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      child: Text(
                                        messages[index].dateTime.day.toString() + " " +
                                            date(messages[index].dateTime) + ", " +
                                            messages[index].dateTime.hour.toString() +
                                            ":" + messages[index].dateTime.minute.toString(),
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ),
                            ],
                          )
                        ),
                      );
                    },
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                  color: Color(0xffe4e4e4),
                ),
                margin: EdgeInsets.all(10),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      child: AutoDirection(
                        text: currentText,
                        child: TextField(
                          controller: msgTextController,
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          onChanged: (t) {
                            setState(() {
                              currentText = msgTextController.text.toString();
                            });
                            sendTyping();
                          },
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(10),
                            border: InputBorder.none,
                            hintText: 'Type Here ...',
                          ),
                          textAlign: TextAlign.justify
                        ),
                      ),
                    ),
                    IconButton(
                      splashColor: Colors.transparent,
                      onPressed: (){
                        send();
                      },
                      icon: Icon(Icons.send),
                      iconSize: 30,
                    )
                  ],
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}