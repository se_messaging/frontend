import 'package:flutter/material.dart';
import 'package:howzit/components/Contact.dart';
import 'package:howzit/components/SearchContactCard.dart';


class Search extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Container(
          margin: EdgeInsets.only(top: 30.0, bottom: 0.0, right:0.0, left: 50.0),
          height: 40,
          width: 40,
          decoration: ShapeDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment(0.3,0.3),
              colors: [ Color(0xFF999999), Color(0xFF1DFDF)], 
          ),
            shape: CircleBorder(),
          ),
        child: IconButton(
            icon: Icon(Icons.search),
            color: Colors.black12,
            onPressed: () {
              showSearch(context: context, delegate: DataSearch());
            },
          ),
        );
  }
}
class DataSearch extends SearchDelegate<String> {

  List<dynamic> suggestionList = Contact.contacts.toList();
  List<Contact> contacts;
  List recent = List() ;

  DataSearch() : super() {
    contacts = Contact.contacts.toList();
  }

  @override
  List<Widget> buildActions(BuildContext context){
    return [IconButton(
      icon: Icon(Icons.clear),
      onPressed: (){
        query = "";
    })
    ];
  }

  @override
  Widget buildLeading(BuildContext context){
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation),
       onPressed: (){
         close(context, null);
       });

  }

  @override
  Widget buildSuggestions(BuildContext context) {
    suggestionList = query.isEmpty?recent:contacts.where((contact) => contact.fullName.toLowerCase().startsWith(query.toLowerCase())).toList();

    return Container(
      margin: EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0)
        ),
      ),
      child: ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0),
              topRight: Radius.circular(30.0)
          ),
          child: ListView.builder(
            itemCount: suggestionList.length,
            itemBuilder: (context, index) {
              return SearchContactCard(contact:suggestionList[index],);
            },
          )
      ),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
  }
}
