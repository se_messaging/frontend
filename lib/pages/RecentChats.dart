import 'package:flutter/material.dart';
import 'package:howzit/components/Contact.dart';
import 'package:howzit/components/ContactCard.dart';
import 'package:howzit/components/Message.dart';
import 'package:howzit/main.dart';

 class RecentChats extends StatefulWidget{
  @override
  _RecentChatsState createState() => _RecentChatsState();
}

class _RecentChatsState extends State<RecentChats> {
   List<Map<String, dynamic>> contacts = List();

   @override
  void initState() {
     loadContact();
     Message.subscribe(update);
     Contact.subscribe(update);
     super.initState();
  }

  void update() {
     loadContact();
  }

   void loadContact() {
     setState(() {
       contacts.clear();
       Contact.contacts.toList().forEach((_contact) {
         var _chat = Message.chatsOf(myPublicKey, _contact.publicKey).toSet().toList();

         contacts.add({
           'contact': _contact,
           'lastPm': _chat.length > 0 ? _chat.first : Message(),
           'toBeSeen': _chat
               .where((element) => element.status == 'sent' && element.to == myPublicKey)
               .length
         });
       });

       contacts.sort((a, b) => - a['lastPm'].dateTime.millisecondsSinceEpoch + b['lastPm'].dateTime.millisecondsSinceEpoch );

     });
   }

   @override
  void dispose() {
     Message.unsubscribe(update);
     Contact.unsubscribe(update);

    super.dispose();
  }

   @override
   Widget build(BuildContext context) {

     return  Container(
       decoration: BoxDecoration(
         color: Colors.white,
         borderRadius: BorderRadius.only(
             topLeft: Radius.circular(30.0),
             topRight: Radius.circular(30.0)
         ),
       ),
       child: ClipRRect(
           borderRadius: BorderRadius.only(
               topLeft: Radius.circular(30.0),
               topRight: Radius.circular(30.0)
           ),
           child: ListView.builder(
             itemCount: contacts.length,
             itemBuilder: (context, index) {
               return ContactCard(contactInfo: contacts[index],);
             },
           )
       ),
     );
   }
}