import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:howzit/components/Helpers.dart';
import 'package:howzit/main.dart';
import 'package:howzit/pages/HomePage.dart';

import 'data.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Tutorial extends StatefulWidget{
  @override
  _Tutorial createState() => _Tutorial();
}


class _Tutorial extends State<Tutorial> {
  List<SliderModel> slides = new List<SliderModel>();
  bool initialized = false;
  int currentIndex = 0;
  PageController pageController = new PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
    init();
    slides = getSlides();

  }

  Future<void> init() async {
    await initAllSystems();
    setState(() {
      initialized = true;
    });
  }

  Widget pageIndexIndicator(bool isCurrentPage){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      height: isCurrentPage ? 10.0 : 6.0,
      width: isCurrentPage ? 10.0 : 6.0,
      decoration: BoxDecoration(
        color: isCurrentPage ? Colors.grey : Colors.grey[500],
        borderRadius: BorderRadius.circular(12),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.greenAccent,
      body: Container(
        child: Column(
          children: [
            Expanded(
              flex: 3,
              child: initialized ? InkWell (
                splashColor: Colors.lightGreenAccent,
                onTap: (){
                  navigate(HomePage(), true);
                },
                child: SizedBox.expand(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    Icon(
                        Icons.check
                    )
                  ],
                 ),
                )
              )
                 : Container(
                  color: Colors.greenAccent,
                  alignment: Alignment.topCenter,
                  child:
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                        ),
                        Text("First Time Setup, This Might Take a While", style:
                        TextStyle(
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        SpinKitThreeBounce(
                          color: Colors.black,
                          size: 20.0,
                        )
                      ],
                    ),
                ),
              ),
            Expanded(
              flex: 17,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(80.0),
                    topRight: Radius.circular(80.0)
                  ),
                    color: Colors.white,
                ),
                child: Column(
                  children: [
                    Expanded(
                      flex: 15,
                      child: PageView.builder(
                          controller: pageController,
                          itemCount: slides.length,
                          onPageChanged: (val){
                            setState(() {
                              currentIndex = val;
                            });
                          },
                          itemBuilder: (context,index){
                            return SliderTitle(
                              imageAssetPath: slides[index].getImageAssetPath(),
                              title: slides[index].getTitle(),
                              description: slides[index].getDescription(),
                            );
                          }),
                    ),

                    Expanded(
                      flex: 4,
                      child: Visibility(
                        visible: initialized ? true : false,
                        child: Container(
                          color: Colors.greenAccent,
                            child: InkWell(
                                splashColor: Colors.lightGreenAccent,
                                onTap: (){
                                  navigate(HomePage(), true);
                                },
                                child: SizedBox(
                                  width: 150,
                                  height: 50,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(padding: EdgeInsets.only(top: 22)),
                                      Text("GET STARTED", style:
                                      TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 17
                                      ),
                                      ),
                                    ],
                                  ),
                                )
                            ),
                          ),
                      )
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomSheet: Container(
        color: Colors.white,
        height: Platform.isIOS ? 70 : 60,
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            InkWell(
              onTap: (){
                pageController.animateToPage(currentIndex - 1, duration: Duration(microseconds: 400), curve: Curves.linear);
              },
              child: Text("PREV", style: TextStyle(
                  color: currentIndex == 0 ? Colors.grey : Colors.black,
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
            Row(
              children: <Widget>[
                for (int i = 0; i < slides.length; i++)
                  currentIndex == i ? pageIndexIndicator(true)
                      : pageIndexIndicator(false)
              ],
            ),
            InkWell(
              onTap: (){
                pageController.animateToPage(currentIndex + 1, duration: Duration(microseconds: 400), curve: Curves.linear);
              },
              child: Text("NEXT", style: TextStyle(
                  color: currentIndex == slides.length - 1? Colors.grey : Colors.black,
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}

class SliderTitle extends StatelessWidget{
  final String imageAssetPath, title, description;
  SliderTitle({this.imageAssetPath, this.title, this.description});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(imageAssetPath, scale: 1.2,),
          Container(
            margin: EdgeInsets.all(20),
            child: Text(description, textAlign: TextAlign.center, style: TextStyle(
                fontWeight: FontWeight.bold
              ),
            ),
          )
        ],
      ),
    );
  }

}